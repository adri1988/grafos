package operacionesSobreGrafos;

import grafos.grafo;

public class util {
	
	public static int degree(grafo G, int v)
	{
	 int degree = 0;
	 for (int w : G.adj(v))
		 degree++;
	 return degree;
	}

}
