package operacionesSobreGrafos;

import java.util.LinkedList;
import java.util.Queue;

import grafos.grafo;

public class BreadthFirstPaths{
	

	private boolean[] marked; // Is a shortest path to this vertex known?
	private int[] edgeTo; // last vertex on known path to this vertex
	private final int s; // source
	
	public BreadthFirstPaths(grafo G, int s){
		marked = new boolean[G.V()];
		edgeTo = new int[G.V()];
		this.s = s;
		bfs(G, s);
	}
	
	private void bfs(grafo G, int s){	
		
	Queue<Integer> q = new LinkedList<Integer>();
	marked[s] = true; // Mark the source
	q.add(s); // and put it on the queue.
	
	while (!q.isEmpty()){
		int v = q.poll(); // Remove next vertex from the queue.
		for (int w : G.adj(v))
		if (!marked[w]){ // For every unmarked adjacent vertex,	
			edgeTo[w] = v; // save last edge on a shortest path,
			marked[w] = true; // mark it because path is known,
			q.add(w); // and add it to the queue.
		}
	}
	}
}
