package grafos;

import java.util.Iterator;

import utils.Bag;

public class grafo {
	
		private final int V;
	    private Bag<Integer>[] adj;
	
		public grafo (int V){//creamos un grafo vac�o 
			 this.V = V;
			 adj = (Bag<Integer>[]) new Bag[V];
			 for (int v = 0; v < V; v++)
			 adj[v] = new Bag<Integer>();
		}
		
		public void addEdge(int v, int w){ // a�ade la arista v-w
			
			adj[v].add(w);
			adj[w].add(v);
			
		}
		
		public Iterable<Integer> adj(int v){ //vertices adjuntos a V
			 return adj[v]; 			
		}
		
		public int V(){//n�mero de vertices
			return V;
		}
		
		public String toString(){
			String res = "";
			for (int i=0;i<V;i++){				
				Iterator<Integer> it = adj[i].iterator();
				res = res + i;
				while (it.hasNext()){
					Integer aux = it.next();
					res  = res + "-" + aux; 
				}
				res += "\n";
			}
				
			
			return res;
		}

}
