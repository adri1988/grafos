package Main;

import grafos.grafo;
import operacionesSobreGrafos.DepthFirstPaths;

public class testDFS {
	
	private static grafo G;
	private static DepthFirstPaths recorrido;
	
	public static void main(String[] args) {
		G = new grafo(7);
		G.addEdge(0, 1);
		G.addEdge(0, 2);
		G.addEdge(0, 5);
		G.addEdge(0, 6);
		G.addEdge(3, 5);
		G.addEdge(3, 4);
		G.addEdge(4, 6);
		G.addEdge(4, 5);
		
		System.out.println(G);
		
		recorrido = new DepthFirstPaths(G,0);		
		for (int v = 1; v < G.V(); v++)
			if (recorrido.hasPathTo(v))
				System.out.println("Recorrdio de 0 a " + v +" " + recorrido.pathTo(v) + "\n");
		
		

	}

}
